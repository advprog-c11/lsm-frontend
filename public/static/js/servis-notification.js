$(document).ready(function () {
	$.get('/components/servis-notification.html', function(data) {
	    $('#servis-notification-modal').html(data);
	});
	connect();
});

function connect() {
    var socket = new SockJS(api_host + '/servis-websocket');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/message/' + localStorage.getItem("username"),
		function (message) {
			showNotification(message.body);
			if (window.location.href.includes('status')) {
			    updateStatus(localStorage.getItem("access_token"));
			}
		});
    });
}

navigator.serviceWorker.register('/static/js/sw.js');
function showNotification(message) {
    if (window.Notification) {
        Notification.requestPermission(function(status) {
			if (status === "granted") {
				var options = {
					body: message,
					dir: 'ltr',
					image: getMotorImageLink(message),
					icon: getMotorImageLink(message)
				};
				navigator.serviceWorker.ready.then(function(registration) {
					registration.showNotification('Layanan Servis Motor', options);
				});
				new Notification('Layanan Servis Motor', options);
			}
        });
    }
    else {
    	showNotificationModal(message);
    }
}

function showNotificationModal(message) {
	$('#servis-notification-body').text(message);
	$('#servis-notification').modal('show');
}

function getMotorImageLink(message) {
	var link = '/static/img/';
	if (message.includes('bebek')) {
		link += 'Bebek.jpg';
	} else if (message.includes('kopling')) {
		link += 'Kopling.jpg';
	} else {
		link += 'Matic.jpg';
	}
	return link;
}
