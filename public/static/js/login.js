$("#login-form").submit(function (e) {
  e.preventDefault();
  var login_username = $("#input-username").val();
  var login_password = $("#input-password").val();
  $.ajax({
    url: api_host + "/oauth/token",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": "Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0"
    },
    data: {
      username: login_username,
      password: login_password,
      grant_type: "password"
    },
    type: "POST"
  }).done(function (result) {
    localStorage.setItem("access_token", result.access_token);
    localStorage.setItem("isLoggedIn", "true");
	localStorage.setItem("username", login_username);
    window.location.replace("/riwayat");
  }).fail(function (result) {
    $('#error-message').text("Invalid username or password.");
  });
});
