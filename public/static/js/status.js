$(document).ready(function () {
  const accessToken = localStorage.getItem("access_token");
  fillStatus(accessToken);
});

function fillStatus(accessToken) {
    $.ajax({
    url: api_host + "/status?id="+sessionStorage.servisId+"&access_token=" + accessToken,
    headers: {
      "Content-Type": "application/json"
    },
    type: "GET",
    success: function (result) {
      $('#gambar-motor').attr({'src': '/static/img/' + result.motor.tipe + '.jpg'})
      $('#tipe-motor').text(result.motor.tipe);
      $('#plat-nomor').text(result.motor.platNomor);
      $('#status-bayar').text((result.sudahDibayar ? 'Sudah' : 'Belum') + ' dibayar');
      var status;
      if (result.status == "1") status = "Cek Rem";
      else if (result.status == "2") status = "Cek Aki";
      else if (result.status == "3") status = "Cek Mesin";
      else if (result.status == "4") status = "Cek Ban";
      else if (result.status == "5") {
        status = "Selesai";
        $('#servis-progress').removeClass('progress-bar-animated');
      };
      $('#status-servis').text(status);
      $('#servis-progress').css('width', parseInt(result.status)/0.05+'%')
    },
    error: function (result) {
      window.location.replace("/login");
    }
  });
}

function updateStatus(accessToken) {
    $.ajax({
    url: api_host + "/status?id="+sessionStorage.servisId+"&access_token=" + accessToken,
    headers: {
      "Content-Type": "application/json"
    },
    type: "GET",
    success: function (result) {
      var status;
      if (result.status == "1") status = "Cek Rem";
      else if (result.status == "2") status = "Cek Aki";
      else if (result.status == "3") status = "Cek Mesin";
      else if (result.status == "4") status = "Cek Ban";
      else if (result.status == "5") status = "Selesai"; 
      $('#status-servis').text(status);
      $('#servis-progress').css('width', parseInt(result.status)/0.05+'%')
    },
    error: function (result) {
      window.location.replace("/login");
    }
  });
}
