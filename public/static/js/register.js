localStorage.setItem("activeNav", "register");
if (localStorage.getItem("isLoggedIn") === "true") {
  window.location.replace("/riwayat");
}

$("#registration-form").submit(function (e) {
  e.preventDefault();
  const registrationData = {
    username: $("#input-username").val(),
    password: $("#input-password").val()
  }
  $.ajax({
    url: api_host + "/register",
    headers: {
      "Content-Type": "application/json"
    },
    data: JSON.stringify(registrationData),
    type: "POST",
    success: function (result) {
      alert("Pendaftaran Sukses");
      window.location.replace("/login");
    },
    error: function (result) {
      alert("Pendaftaran Gagal");
    }
  });
});
