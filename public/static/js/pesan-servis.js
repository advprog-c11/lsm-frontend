if (localStorage.getItem('isLoggedIn') != 'true') {
  window.location.replace('/login');
}

localStorage.setItem('activeNav', 'order');
$("#pesan-servis-form").submit(function (e) {
  e.preventDefault();
  const accessToken = localStorage.getItem("access_token");
  const pesanServisData = {
    tipe: $("#input-tipe").val(),
    platNomor: $("#input-platNomor").val(),
    merek: $("#input-merek").val(),
    warna: $("#input-warna").val(),
  };
  $.ajax({
    url: api_host + "/pesan-servis?access_token=" + accessToken,
    headers: {
      "Content-Type": "application/json"
    },
    data: JSON.stringify(pesanServisData),
    type: "POST",
    success: function (result) {
      alert("Pemesanan sukses!");
      $("#pesan-servis-form").clear();
    },
    error: function (result) {
      alert("Pemesanan gagal!");
    }
  });
});
