localStorage.setItem("activeNav", "history");
function createRow(element) {
  const id = $('<th>').attr({ 'scope': 'row' }).text(element.id);
  const platNomor = $('<td>').text(element.motor.platNomor);
  const tipe = $('<td>').text(element.motor.tipe);
  const status = $('<td>').text(
    (element.sudahDibayar ? 'Sudah' : 'Belum') + ' dibayar'
  );
  const detail = $('<td>').append($('<button>').attr(
    { 'type': 'button', 'onclick': 'checkDetail(' + element.id + ')' }
  ).addClass('btn btn-primary detail-button').text('Detail'));
  $('<tr>').append(id, platNomor, tipe, status, detail).appendTo(
    $('#tbody-riwayat')
  );
}

function checkDetail(servisId) {
  sessionStorage.servisId = servisId;
  window.location.replace("/status")
}

$(document).ready(function () {
  const accessToken = localStorage.getItem("access_token");
  $.ajax({
    url: api_host + "/riwayat?access_token=" + accessToken,
    headers: {
      "Content-Type": "application/json"
    },
    type: "GET",
    success: function (result) {
      if (result.length > 0) {
        result.forEach(element => {
          createRow(element);
        });
        $('#riwayat-table').DataTable();
        inlineDataTable();
      } else {
        $('#riwayat').remove();
        $('#no-servis').removeClass('d-none');
      }
    },
    error: function (result) {
      window.location.replace("/login");
    }
  });
});

function inlineDataTable() {
  const riwayatTableLength = $('#riwayat-table_length');
  riwayatTableLength.find('label').addClass('form-inline');
  riwayatTableLength.find('select').addClass('form-control form-inline mx-3');
  const riwayatTableFilter = $('#riwayat-table_filter');
  riwayatTableFilter.find('label').addClass('form-inline');
  riwayatTableFilter.find('input').addClass('form-control form-inline mx-3');
}
