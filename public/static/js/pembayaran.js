$('#bayar-modal-toggle').click(function() {
  $.ajax({
    url: api_host + "/status?id=" + sessionStorage.servisId
      + "&access_token=" + localStorage.access_token,
    type: "GET",
    success: function (result) {
      $('#nominal').text(result.harga);
    }
  });
})

$('#metode-pembayaran').change(function () {
  if ($('#metode-pembayaran').val() == "Tunai") {
    $('#detail-kartu').addClass('d-none');
    $('#nomor-kartu').prop('required', false);
  } else {
    $('#detail-kartu').removeClass('d-none');
    $('#nomor-kartu').prop('required', true);
  }
});

$('#button-bayar').click(function (e) {
  e.preventDefault();
  $('#error-bayar').text('');
  const pembayaranData = {
    idServis: sessionStorage.getItem("servisId"),
    tipe: $('#metode-pembayaran').val(),
    nomorKartu: $('#nomor-kartu').val()
  };
  $.ajax({
    url: api_host + '/bayar?access_token=' + localStorage.getItem('access_token'),
    data: JSON.stringify(pembayaranData),
    contentType: 'application/json',
    dataType: 'json',
    type: 'POST'
  }).done(function (result) {
    console.log(result);
    if (result.success) {
      $('#modal-bayar').modal('hide');
      $('#error-bayar').text('');
      $('#status-bayar').text('Sudah dibayar');
    } else {
      $('#error-bayar').text(result.response);
    };
  }).fail(function (result) {
    if (result.responseJSON.message) {
      $('#error-bayar').text(result.responseJSON.message);
    } else {
      $('#error-bayar').text('Terjadi kesalahan. Silakan coba lagi.');
    }
  });
});
