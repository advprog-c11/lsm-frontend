$.get('/components/navbar.html', function(data) {
    $('#navbar').html(data);
    if (localStorage.getItem('isLoggedIn') === 'true') {
        $('#nav-login').remove();
        $('#nav-register').remove();
        $('#nav-logout').click(function() {
            localStorage.setItem('isLoggedIn', 'false');
            localStorage.removeItem('access_token');
            window.location.replace('/');
        });
    } else {
        $('#nav-order').remove();
        $('#nav-history').remove();
        $('#nav-logout').remove();
    }
    const activeNav = localStorage.getItem('activeNav');
    $('#nav-' + activeNav).addClass('active');
});
