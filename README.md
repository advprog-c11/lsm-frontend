# Layanan Servis Motor (LSM) Front-end

[![Netlify Status](https://api.netlify.com/api/v1/badges/e6532839-30a3-4aa9-af9e-473b984545fb/deploy-status)](https://app.netlify.com/sites/lsm/deploys)

## [Live site](https://lsm.netlify.com)

##### Advanced Programming - C
##### Kelompok 11

| NPM        | Nama                       |
| ---------- | ----                       |
| 1706039383 | Muhammad Nadhirsyah Indra  |
| 1706979152 | Ahmad Fauzan Amirul Isnain |
| 1606917765 | Zunino Sultan Anggara      |
| 1706979455 | Sage Muhammad Abdullah     |
| 1406623865 | Irvanno Taqi Irmawan       |
